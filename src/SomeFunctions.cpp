#include <iostream>
#include <algorithm>
#include "../inc/someFunctions.hpp"

namespace an
{
    void Read(int n, int a[100][100])
    {
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                std::cin >> a[i][j];
    };

    bool hasEqualRows(int n, int a[100][100])
    {
        for (int i = 0; i < n - 1; ++i)
            for (int j = i + 1; j < n; ++j)
            {
                for (int k = 0; k < n; ++k)
                    if (a[i][k] != a[j][k])
                        return false;
                return true;
                if (true)
                    return true;
            }
        return false;
    };

    int minsOfColumns(int n, int min, int a[100][100])
    {
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            {
                min = a[0][j];
                if (min > a[i][j])
                    std::swap(min, a[i][j]);
                return min;
            }   
    };

    bool isPrime(int x)
    {
        if (x < 2)
            return false;
        for (int d = 2; d <= sqrt(x); d++)
            if (x % d == 0)
                return false;
        return true;
    };

    bool consistsPrime(int n, int a[100][100])
    {
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (an::isPrime(a[i][j]))
                    return true;
        return false;
    };

    int maxPrime(int n, int mp, int a[100][100], int size)
    {
        int primes[10000];
        size = 0;
        mp = 0;
        if (an::consistsPrime(n, a))
        {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    bool exists = false;
                    for (int k = 0; k < size; k++)
                        if (primes[k] == a[i][j])
                            if (an::isPrime(primes[k]))
                            {
                                exists = true;
                                break;
                            }
                    if (!exists)
                    {
                        primes[size] = a[i][j];
                        if (an::isPrime(primes[size]))
                            size++;
                    }
                    for (int k = 0; k < size; k++)
                    {
                        if (mp < primes[k])
                            std::swap(mp, primes[k]);
                    }
                }       
        }
        return mp;
    };

    void Write(int n, int a[100][100])
    {
        std::cout << std::endl;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
                std::cout << a[i][j] << " ";
            std::cout << std::endl;
        } 
    };
}