﻿#include <iostream>
#include "inc/someFunctions.hpp"

int main()
{
    int a[100][100];
    int n;
    int min = a[0][0]; 
    int mp = 0;
    int size = 0;
    std::cin >> n;
    an::Read(n, a);
    if (an::consistsPrime(n, a))
    {
        an::maxPrime(n, mp, a, size);
        if (an::hasEqualRows(n, a))
        {
            an::minsOfColumns(n, min, a);
            min = mp;
        }
    }  
    an::Write(n, a);
    return 0;
}