#pragma once

namespace an
{
	void Read(int n, int a[100][100]);

	bool hasEqualRows(int n, int a[100][100]);

	int minsOfColumns(int n, int min, int a[100][100]);

	bool isPrime(int x);

	bool consistsPrime(int n, int a[100][100]);

	int maxPrime(int n, int mp, int a[100][100], int size);

	void Write(int n, int a[100][100]);
}